import React from 'react';
import { TouchableOpacity } from 'react-native';

const Touch = ({
  style = {}, 
  onPress = () => {},
  children,
  activeOpacity = 0.8
}) => {
  return (
    <TouchableOpacity 
      activeOpacity={activeOpacity} 
      underlayColor = 'transparent' 
      style={style} 
      onPress={onPress} 
    >
      {children}
    </TouchableOpacity>
  )
}

export default Touch;