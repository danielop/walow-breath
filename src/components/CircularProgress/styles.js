import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window')

const sizeContainer = width * .75
const sizeBlurCircle = .8 * sizeContainer

const styles = StyleSheet.create({
  container: {
    width: sizeContainer,
    height: sizeContainer,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  blurCircle: {
    width: sizeBlurCircle,
    height: sizeBlurCircle,
    borderRadius: .5 * (sizeBlurCircle),
    backgroundColor: 'rgba(123, 102, 255, 0.4)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  purpleCircle: {
    width: sizeBlurCircle - 30,
    height: sizeBlurCircle - 30,
    borderRadius: .5 * (sizeBlurCircle - 30),
    backgroundColor: 'rgba(123, 102, 255, 0.8)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    resizeMode: 'contain',
    width: '30%',
    height: '30%',
  },
  containerProgress: {
    position: 'absolute',
    zIndex: 1,
    width: sizeContainer * .95,
    height: sizeContainer * .95,
    alignItems: 'center',
    justifyContent: 'center'
  },
  middleMark: {
    backgroundColor: '#6D61FA',
    borderColor: 'white',
    borderWidth: 2,
    height: 18,
    width: 10,
    borderRadius: 6,
    position: 'absolute',
    zIndex: 2,
    top: (sizeContainer * .95) - 9
  }
});

export default styles