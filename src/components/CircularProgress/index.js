import React from 'react';
import { View, Animated, Dimensions, Easing } from 'react-native';
import { LogoBlack, LogoWhite } from '../../assets';
import styles from './styles';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Image } from 'react-native-svg';

const CircularProgressComponent = ({start, isInhale, progress}) => {
  const { width } = Dimensions.get('window')
  const sizeContainer = width * .75
  const sizeBlurCircle = .8 * (sizeContainer - 30)
  const sizeProgress = sizeContainer * .95

  const scaleAnim = React.useRef(new Animated.Value(sizeBlurCircle * .3)).current
  const [showProgress, setShowProgress] = React.useState(true)

  React.useEffect(() => {
    if (start) {
      Animated.timing(scaleAnim, {
        toValue: sizeBlurCircle * (isInhale ? .5 : .3),
        duration: 5000,
        useNativeDriver: false
      }).start()
    } else {
      Animated.timing(scaleAnim, {
        toValue: sizeBlurCircle * 0.3,
        duration: 500,
        useNativeDriver: false
      }).start()
    }
  }, [start, isInhale])

  React.useEffect(() => {
    if (progress === 0 || !start) {
      setShowProgress(false)
      setTimeout(() => setShowProgress(true), 10);
    }
  }, [progress, start])
  
  return <View style={styles.container}>
    <View style={styles.blurCircle}>
      <View style={styles.purpleCircle}>
        <Animated.Image
          source={start ? LogoWhite : LogoBlack}
          style={[styles.logo,  {width: scaleAnim, height: scaleAnim}]}
        />
      </View>
    </View>
    {showProgress ? <AnimatedCircularProgress
      style={styles.containerProgress}
      size={sizeProgress}
      width={4}
      fill={progress}
      tintColor="rgba(15, 16, 32, 0.4)"
      backgroundColor="#C0BDE5"
      rotation={0}
      padding={9}
      duration={1000}
      easing={Easing.out(Easing.linear)}
      skipAnimOnComplete={true}
      renderCap={({ center }) => <Image
        href={require('../../assets/icons/indicator-progress.png')}
        width={18}
        height={18}
        x={center.x - 9}
        y={center.y - 9}
      />}
    /> : null}
    {start ? <View style={styles.middleMark} /> : null}
  </View>
}

export default CircularProgressComponent;