import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Animated
} from 'react-native';

import Touch from '../../components/Touch';
import CircularProgress from '../../components/CircularProgress';
import LinearGradient from 'react-native-linear-gradient';
import { AlarmClock, Play, Back, Pause } from '../../assets';
import styles from './styles';

const BreathPage = () => {
  const times = [60, 120, 180]
  const intervalTime = 1000

  const [selectedTime, setSelectedTime] = React.useState(times[0])
  const [isInhale, setIsInhale] = React.useState(true)
  const [currentTime, setCurrentTime] = React.useState(times[0])
  const [played, setPlayed] = React.useState(false)
  const [timer, setTimer] = React.useState(null)
  const [progress, setProgress] = React.useState(0)

  const fadeAnim = React.useRef(new Animated.Value(0)).current;

  const resetTime = (time) => {
    setSelectedTime(time)
    setCurrentTime(time)
    setPlayed(false)
    setProgress(0)
    setIsInhale(true)
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 50,
      useNativeDriver: false
    }).start()
  }

  const renderTimes = () => {
    return times.map(time => <Touch
      key={'time-item' + time}
      onPress={() => resetTime(time)}
    >
      <View style={[styles.itemTime, selectedTime !== time ? styles.disabledTime : {}]}>
        <AlarmClock width={18} height={18} />
        <Text style={styles.labelTime}>
          {Math.floor(time / 60)} min
        </Text>
      </View>
    </Touch>)
  }

  const renderTimer = () => {
    const minutes = Math.floor(currentTime / 60)
    const seconds = Math.floor(currentTime % 60)
    return `${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`
  }

  const onPlay = () => {
    setPlayed(ov => !ov)
    Animated.timing(fadeAnim, {
      toValue: played ? 0 : 1,
      duration: 500,
      useNativeDriver: false
    }).start()
  }

  React.useEffect(() => {
    if (played && currentTime > 0) {
      setTimer(setTimeout(() => {
        setCurrentTime(ov => (ov - (intervalTime / 1000)) < 0 ? 0 : ov - (intervalTime / 1000))
        setProgress(ov => (ov + (intervalTime / 100)) > 100 ? 0 : ov + (intervalTime / 100))
      }, intervalTime))
    }
    return () => clearTimeout(timer)
  }, [currentTime])

  React.useEffect(() => {
    if (progress === 0) {
      setIsInhale(true)
      setProgress(10)
    } else {
      setIsInhale(progress > 50 ? false : true)
    }
  }, [progress])

  React.useEffect(() => {
    if (played) {
      if (currentTime === selectedTime) {
        // First second
        setCurrentTime(ov => ov - (intervalTime / 1000))
        setProgress(ov => ov + (intervalTime / 100))
        setIsInhale(true)
      } else if (currentTime === 0) {
        setTimeout(() => {
          clearTimeout(timer)
          resetTime(selectedTime)
        }, 1000);
      }
      // Nothing more
    } else {
      clearTimeout(timer)
      resetTime(selectedTime)
    }
  }, [played, currentTime])

  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#BCBCC7', '#7B66FF']}
        style={styles.linearGradient}
      />
      <Touch
        style={styles.backBtn}
        onPress={() => resetTime(selectedTime)}
      >
        <Back width={7} height={19} />
      </Touch>
      <Text style={styles.title}>
        {'Breathe & relax'}
      </Text>
      <Animated.Text style={[styles.actionLabel, {opacity: fadeAnim}]}>
        {isInhale ? 'Inhale' : 'Exhale'}
      </Animated.Text>
      <CircularProgress
        isInhale={isInhale}
        start={played}
        progress={progress}
      />
      <Animated.Text style={[styles.actionLabel, {opacity: fadeAnim}]}>
        {renderTimer()}
      </Animated.Text>
      <Touch
        style={styles.btnPlay}
        onPress={onPlay}
      >
        {played ? <Pause width={60} height={60} /> : <Play width={60} height={60} />}
      </Touch>
      <View style={styles.rowTimes}>
        {renderTimes()}
      </View>
    </SafeAreaView>
  );
};



export default BreathPage;
