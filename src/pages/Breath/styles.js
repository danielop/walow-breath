import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  },
  linearGradient: {
    position: 'absolute',
    zIndex: 0,
    width,
    height
  },
  backBtn: {
    padding: 18,
    marginLeft: 12,
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
  actionLabel: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  timeLabel: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 12
  },
  btnPlay: {
    alignSelf: 'center',
    padding: 18
  },
  rowTimes: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 60
  },
  itemTime: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 6,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 12
  },
  labelTime: {
    color: 'white',
    fontSize: 14,
    marginLeft: 3
  },
  disabledTime: {
    opacity: 0.35,
    backgroundColor: 'rgba(15, 16, 32, 0.7)'
  }
});

export default styles