import AlarmClock from './icons/alarm-clock.svg'
import Play from './icons/play.svg'
import Back from './icons/back.svg'
import Pause from './icons/pause.svg'
import LogoWhite from './icons/logo-white.png'
import LogoBlack from './icons/logo-black.png'

export {
  AlarmClock,
  Play,
  Back,
  Pause,
  LogoWhite,
  LogoBlack,
}